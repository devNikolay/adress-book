package com.homework.adressbook.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.homework.adressbook.R;
import com.homework.adressbook.data.User;
import com.homework.adressbook.db.tables.UserTables;

import butterknife.Bind;
import butterknife.ButterKnife;

public class UserListAdapter extends CursorAdapter {

    private LayoutInflater mCursorInflater;

    public UserListAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.mCursorInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = mCursorInflater.inflate(R.layout.list_item_user, null);
        ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        User user = new User(cursor);
        holder.tvNameUser.setText(user.getName());
        holder.tvEmailUser.setText(user.getEmail());
        holder.tvPhoneUser.setText(user.getPhone());
        holder.tvStreetUser.setText(user.getStreet());
        holder.tvCityUser.setText(user.getCity());
        holder.tvStateUser.setText(user.getState());
        holder.tvZipUser.setText(user.getZip());
    }

    @Override
    public Long getItem(int position) {
        return getCursor().getLong(getCursor().getColumnIndex(UserTables.Columns.COLUMN_ID));
    }

    static class ViewHolder {
        @Bind(R.id.tv_name_user)
        TextView tvNameUser;
        @Bind(R.id.tv_email_user)
        TextView tvEmailUser;
        @Bind(R.id.tv_phone_user)
        TextView tvPhoneUser;
        @Bind(R.id.tv_street_user)
        TextView tvStreetUser;
        @Bind(R.id.tv_city_user)
        TextView tvCityUser;
        @Bind(R.id.tv_state_user)
        TextView tvStateUser;
        @Bind(R.id.tv_zip_user)
        TextView tvZipUser;

        public ViewHolder(View v) {
            ButterKnife.bind(this, v);
        }
    }
}
