package com.homework.adressbook.db.tables;

public abstract class UserTables {

    public interface Columns {
        String NAME = "name";
        String PHONE = "phone";
        String EMAIL = "email";
        String STREET = "street";
        String CITY = "city";
        String STATE = "state";
        String ZIP = "zip";
        String COLUMN_ID = "_id";
    }

    public interface Requests {

        String TABLE_NAME = "contacts";


        String CREATION_REQUEST = "CREATE TABLE " + TABLE_NAME
                + " ( "
                + Columns.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Columns.NAME + " TEXT, "
                + Columns.PHONE + " TEXT, "
                + Columns.EMAIL + " TEXT, "
                + Columns.STREET + " TEXT, "
                + Columns.CITY + " TEXT, "
                + Columns.STATE + " TEXT, "
                + Columns.ZIP + " TEXT"
                + " );";

        String DROP_REQUEST = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}
