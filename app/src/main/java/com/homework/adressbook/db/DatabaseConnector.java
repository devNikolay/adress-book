package com.homework.adressbook.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.homework.adressbook.db.tables.UserTables;

import java.sql.SQLException;

public class DatabaseConnector {

    private SQLiteDatabase mDatabase;
    private DatabaseOpenHelper mDatabaseOpenHelper;

    public DatabaseConnector(Context context) {
        mDatabaseOpenHelper = new DatabaseOpenHelper(context, UserTables.Requests.TABLE_NAME, null, 1);
    }

    public void open() throws SQLException {
        mDatabase = mDatabaseOpenHelper.getWritableDatabase();
    }

    public void close() {
        if (mDatabase != null) {
            mDatabase.close();
        }
    }

    public long insertContact(String name, String phone, String email,
                              String street, String city, String state, String zip) throws SQLException {
        ContentValues newContact = new ContentValues();
        newContact.put(UserTables.Columns.NAME, name);
        newContact.put(UserTables.Columns.PHONE, phone);
        newContact.put(UserTables.Columns.EMAIL, email);
        newContact.put(UserTables.Columns.STREET, street);
        newContact.put(UserTables.Columns.CITY, city);
        newContact.put(UserTables.Columns.STATE, state);
        newContact.put(UserTables.Columns.ZIP, zip);

        open();
        long rowID = mDatabase.insert(UserTables.Requests.TABLE_NAME, null, newContact);
        close();
        return rowID;
    }

    public void updateContact(long id, String name, String phone,
                              String email, String street, String city, String state, String zip) throws SQLException {

        ContentValues editContact = new ContentValues();
        editContact.put(UserTables.Columns.NAME, name);
        editContact.put(UserTables.Columns.PHONE, phone);
        editContact.put(UserTables.Columns.EMAIL, email);
        editContact.put(UserTables.Columns.STREET, street);
        editContact.put(UserTables.Columns.CITY, city);
        editContact.put(UserTables.Columns.STATE, state);
        editContact.put(UserTables.Columns.ZIP, zip);

        open();
        mDatabase.update(UserTables.Requests.TABLE_NAME, editContact, "_id=" + id, null);
        close();
    }

    public Cursor getAllContacts() {
        return mDatabase.query(UserTables.Requests.TABLE_NAME, null, null, null, null, null, null, null);
    }

    public Cursor getOneContact(long id) {
        return mDatabase.query(UserTables.Requests.TABLE_NAME, null, "_id = ?", new String[]{String.valueOf(id)}, null, null, null);

    }

    public void deleteContact(long id) throws SQLException {
        open();
        mDatabase.delete(UserTables.Requests.TABLE_NAME, "_id = ?", new String[]{String.valueOf(id)});
        close();
    }

    private class DatabaseOpenHelper extends SQLiteOpenHelper {

        public DatabaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(UserTables.Requests.CREATION_REQUEST);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(UserTables.Requests.DROP_REQUEST);
            onCreate(db);
        }
    }
}
