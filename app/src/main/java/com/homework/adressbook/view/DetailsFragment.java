package com.homework.adressbook.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.homework.adressbook.db.DatabaseConnector;
import com.homework.adressbook.R;
import com.homework.adressbook.db.tables.UserTables;

import java.sql.SQLException;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailsFragment extends Fragment {

    private DetailsFragmentListener listener;

    private long rowID = -1;
    @Bind(R.id.tv_name)
    TextView name;
    @Bind(R.id.tv_phone)
    TextView phone;
    @Bind(R.id.tv_email)
    TextView email;
    @Bind(R.id.tv_street)
    TextView street;
    @Bind(R.id.tv_city)
    TextView city;
    @Bind(R.id.tv_state)
    TextView state;
    @Bind(R.id.tv_zip)
    TextView zip;


    public interface DetailsFragmentListener {
        void onContactDeleted();

        void onEditContact(Bundle arguments);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);

        ((MainActivity)getActivity()).getmFab().hide();

        rowID =  getArguments().getLong(MainActivity.ROW_ID);

        View view = inflater.inflate(R.layout.fragment_details, container,false);
        ButterKnife.bind(this, view);


        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(MainActivity.ROW_ID, rowID);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_details_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                Bundle args = new Bundle();
                args.putLong(MainActivity.ROW_ID, rowID);
                args.putCharSequence(UserTables.Columns.NAME, name.getText());
                args.putCharSequence(UserTables.Columns.PHONE, phone.getText());
                args.putCharSequence(UserTables.Columns.EMAIL, email.getText());
                args.putCharSequence(UserTables.Columns.STREET, street.getText());
                args.putCharSequence(UserTables.Columns.CITY, city.getText());
                args.putCharSequence(UserTables.Columns.STATE, state.getText());
                args.putCharSequence(UserTables.Columns.ZIP, zip.getText());
                listener.onEditContact(args);
                return true;
            case R.id.action_delete:
                deleteContact();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    private void deleteContact() {
        confirmDelete.show(getFragmentManager(), "confirm delete");
    }

    private DialogFragment confirmDelete = new DialogFragment() {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.confirm_title)
                    .setMessage(R.string.confirm_message)
                    .setPositiveButton(R.string.button_delete, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final DatabaseConnector databaseConnector = new DatabaseConnector(getActivity());
                            AsyncTask<Long, Object, Object> deleteTask = new AsyncTask<Long, Object, Object>() {
                                @Override
                                protected Object doInBackground(Long... params) {
                                    try {
                                        databaseConnector.deleteContact(params[0]);
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Object o) {

                                }
                            };
                            deleteTask.execute(new Long[]{rowID});
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    });
            builder.setNegativeButton(R.string.button_cancel, null);
            return builder.create();
        }
    };


    @Override
    public void onResume() {
        super.onResume();
        new LoadContactTask().execute(rowID);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (DetailsFragmentListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private class LoadContactTask extends AsyncTask<Long, Object, Cursor> {

        DatabaseConnector databaseConnector = new DatabaseConnector(getActivity());

        @Override
        protected Cursor doInBackground(Long... params) {
            try {
                databaseConnector.open();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return databaseConnector.getOneContact(params[0]);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            super.onPostExecute(cursor);
            cursor.moveToFirst();

            int nameIndex = cursor.getColumnIndex(UserTables.Columns.NAME);
            int phoneIndex = cursor.getColumnIndex(UserTables.Columns.PHONE);
            int emailIndex = cursor.getColumnIndex(UserTables.Columns.EMAIL);
            int streetIndex = cursor.getColumnIndex(UserTables.Columns.STREET);
            int cityIndex = cursor.getColumnIndex(UserTables.Columns.CITY);
            int stateIndex = cursor.getColumnIndex(UserTables.Columns.STATE);
            int zipIndex = cursor.getColumnIndex(UserTables.Columns.ZIP);

            name.setText(cursor.getString(nameIndex));
            phone.setText(cursor.getString(phoneIndex));
            email.setText(cursor.getString(emailIndex));
            street.setText(cursor.getString(streetIndex));
            city.setText(cursor.getString(cityIndex));
            state.setText(cursor.getString(stateIndex));
            zip.setText(cursor.getString(zipIndex));

            cursor.close();
            databaseConnector.close();

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}
