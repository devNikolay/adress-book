package com.homework.adressbook.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.homework.adressbook.R;
import com.homework.adressbook.db.DatabaseConnector;
import com.homework.adressbook.db.tables.UserTables;

import java.sql.SQLException;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AddEditFragment extends Fragment {

    private Bundle mContactInfoBundle;
    private long rowID;

    @Bind(R.id.et_name)
    EditText mName;
    @Bind(R.id.et_phone)
    EditText mPhone;
    @Bind(R.id.et_email)
    EditText mEmail;
    @Bind(R.id.et_street)
    EditText mStreet;
    @Bind(R.id.et_city)
    EditText mCity;
    @Bind(R.id.et_state)
    EditText mState;
    @Bind(R.id.et_zip)
    EditText mZip;
    @Bind(R.id.btn_saveContactButton)
    Button mSaveContact;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_add_edit, container, false);

        ButterKnife.bind(this, view);
        mContactInfoBundle = getArguments();

        if (mContactInfoBundle != null) {
            rowID = mContactInfoBundle.getLong(MainActivity.ROW_ID);
            mName.setText(mContactInfoBundle.getString(UserTables.Columns.NAME));
            mPhone.setText(mContactInfoBundle.getString(UserTables.Columns.PHONE));
            mEmail.setText(mContactInfoBundle.getString(UserTables.Columns.EMAIL));
            mStreet.setText(mContactInfoBundle.getString(UserTables.Columns.STREET));
            mCity.setText(mContactInfoBundle.getString(UserTables.Columns.CITY));
            mState.setText(mContactInfoBundle.getString(UserTables.Columns.STATE));
            mZip.setText(mContactInfoBundle.getString(UserTables.Columns.ZIP));

        }

        mSaveContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mName.getText().toString().trim().length() != 0) {
                    AsyncTask<Object, Object, Object> saveContactTask = new AsyncTask<Object, Object, Object>() {

                        @Override
                        protected Object doInBackground(Object... params) {
                            saveContact();
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Object o) {
                            super.onPostExecute(o);

                            InputMethodManager imm = (InputMethodManager)
                                    getActivity().getSystemService(
                                            Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(
                                    getView().getWindowToken(), 0);
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    };

                    saveContactTask.execute((Object[]) null);
                } else {
                    DialogFragment erorSaving = new DialogFragment() {
                        @Override
                        public Dialog onCreateDialog(Bundle savedInstanceState) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(R.string.error_message)
                                    .setPositiveButton(R.string.ok, null);
                            return builder.create();
                        }
                    };
                    erorSaving.show(getFragmentManager(), "error saving contact");
                }
            }
        });

        return view;
    }

    private void saveContact() {
        DatabaseConnector dataConnnector = new DatabaseConnector(getActivity());

        if (mContactInfoBundle == null) {
            try {
                rowID = dataConnnector.insertContact(
                        mName.getText().toString(),
                        mPhone.getText().toString(),
                        mEmail.getText().toString(),
                        mStreet.getText().toString(),
                        mCity.getText().toString(),
                        mState.getText().toString(),
                        mZip.getText().toString());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            try {
                dataConnnector.updateContact(rowID,
                        mName.getText().toString(),
                        mPhone.getText().toString(),
                        mEmail.getText().toString(),
                        mStreet.getText().toString(),
                        mCity.getText().toString(),
                        mState.getText().toString(),
                        mZip.getText().toString());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}
