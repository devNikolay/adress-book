package com.homework.adressbook.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.homework.adressbook.R;

public class MainActivity extends BaseActivity implements DetailsFragment.DetailsFragmentListener {

    public static final String ROW_ID = "row_id";
    private FloatingActionButton mFab;
    private UserListFragment userList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new AddEditFragment(), true);
                mFab.hide();
            }
        });

        changeFragment(new UserListFragment(), false);
    }


    private void displayAddEditFragment(int viewId, Bundle args) {
        AddEditFragment addEditFragment = new AddEditFragment();

        if (args != null) // editing existing contact
            addEditFragment.setArguments(args);
        changeFragment(addEditFragment, true);
    }


    public void startDetailFragment(long rowID) {
        if (findViewById(R.id.fragmentContainer) != null)
            displayContact(rowID, R.id.fragmentContainer);

        else {
            getFragmentManager().popBackStack();

        }
    }

    private void displayContact(long rowId, int viewId) {
        DetailsFragment detailsFragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putLong(ROW_ID, rowId);
        detailsFragment.setArguments(args);

        changeFragment(detailsFragment, true);

    }

    @Override
    public void onContactDeleted() {
        getFragmentManager().popBackStack();

        if (findViewById(R.id.fragmentContainer) == null)
            userList.updateContactList();
    }


    @Override
    public void onEditContact(Bundle arguments) {
        if (findViewById(R.id.fragmentContainer) != null)
            displayAddEditFragment(R.id.fragmentContainer, arguments);
    }

    public FloatingActionButton getmFab() {
        return mFab;
    }
}
