package com.homework.adressbook.view;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.homework.adressbook.R;
import com.homework.adressbook.adapter.UserListAdapter;
import com.homework.adressbook.db.DatabaseConnector;

import java.sql.SQLException;

import butterknife.Bind;
import butterknife.ButterKnife;

public class UserListFragment extends Fragment {

    @Bind(R.id.lvUsers)
    ListView lvUsers;

    private UserListAdapter mAdapter;
    private Cursor mCursor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_user_list, container, false);
        ButterKnife.bind(this, v);
        lvUsers.setEmptyView((TextView) v.findViewById(android.R.id.empty));
        ((MainActivity)getActivity()).getmFab().show();

        lvUsers.setOnItemClickListener(viewContactListener);
        lvUsers.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        new Handler().post(new Runnable() {

            @Override
            public void run() {
                mAdapter = new UserListAdapter(getContext(),
                        mCursor,
                        0);

                lvUsers.setAdapter(mAdapter);
            }

        });

        return v;

    }

    AdapterView.OnItemClickListener viewContactListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            ((MainActivity)getActivity()).startDetailFragment(mAdapter.getItemId(position));
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        updateContactList();
    }

    @Override
    public void onStop() {
        Cursor cursor = mAdapter.getCursor();
        mAdapter.changeCursor(null);
        if (cursor != null) {
            cursor.close();
        }
        super.onStop();
    }

    public void updateContactList() {
        new GetContactsTask().execute((Object[]) null);
    }

    private class GetContactsTask extends AsyncTask<Object, Object, Cursor> {


        DatabaseConnector databaseConnector = new DatabaseConnector(getActivity());

        @Override
        protected Cursor doInBackground(Object... params) {
            try {
                databaseConnector.open();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return databaseConnector.getAllContacts();
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            mAdapter.changeCursor(cursor);

            databaseConnector.close();
        }
    }
}
