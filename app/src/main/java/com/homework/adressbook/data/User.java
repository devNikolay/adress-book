package com.homework.adressbook.data;

import android.database.Cursor;

import com.homework.adressbook.db.tables.UserTables;

import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


@EqualsAndHashCode(callSuper = false)
@lombok.Data
@Accessors(prefix = "m")
public class User {

    private String mName;
    private String mPhone;
    private String mEmail;
    private String mStreet;
    private String mCity;
    private String mState;
    private String mZip;

    public User(Cursor c) {
        this.mName = c.getString(c.getColumnIndex(UserTables.Columns.NAME));
        this.mPhone = c.getString(c.getColumnIndex(UserTables.Columns.PHONE));
        this.mEmail = c.getString(c.getColumnIndex(UserTables.Columns.EMAIL));
        this.mStreet = c.getString(c.getColumnIndex(UserTables.Columns.STREET));
        this.mCity = c.getString(c.getColumnIndex(UserTables.Columns.CITY));
        this.mState = c.getString(c.getColumnIndex(UserTables.Columns.STATE));
        this.mZip = c.getString(c.getColumnIndex(UserTables.Columns.ZIP));
    }
}
